/*==================================================================

	====================
	 [ZombieMod] Weapon
	====================
	
	파일: weapon.inc
	언어: SourcePawn
	방식: 절차형
	종류: 함수 집합
	설명: 무기 삭제, 확인 등이 있습니다.

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

/**
 * 무기 이름의 최대길이
 */
#define WEAPON_MAX_LENGTH 32

/**
 * 실제 무기 슬롯의 숫자 (CS:S)
 */
#define WEAPON_SLOTS_MAX 5

/**
 * @section CS:S 시작 무기
 */
#define WEAPON_SPAWN_T_WEAPON "weapon_glock"
#define WEAPON_SPAWN_CT_WEAPON "weapon_usp"
/**
 * @endsection
 */

/**
 * @section 무기 슬롯
 */
enum WeaponsSlot
{
	Slot_Invalid		= -1,	/** 무효한 무기 (슬롯) */
	Slot_Primary		= 0,	/** 주 무기 슬롯 */
	Slot_Secondary		= 1,	/** 보조무기 슬롯 */
	Slot_Melee			= 2,	/** 근접무기 (칼) */
	Slot_Projectile		= 3,	/** 발사물 (폭탄, 플래시뱅 등) */
	Slot_Explosive		= 4,	/** 폭발물 (C4) 무기 슬롯 */
	Slot_NVGs			= 5,	/** NVG (가짜) 무기 슬롯 */
}
/**
 * @endsection
 */

/**
 * 클라이언트의 모든 무기 인덱스를 포함할 수 있는 배열을 반영(?)
 * (Return an array that contains all client's weapon indexes.)
 *
 * @param client		클라이언트 인덱스
 * @param weapons		무기 인덱스 배열.
 * 						슬롯에 무기가 없으면 '-1'
 */
stock Weapon_GetClientWeapons(client, weapons[WeaponsSlot])
{
	// x = 무기 슬롯.
	for (new x = 0; x < WEAPON_SLOTS_MAX; x++)
		weapons[x] = GetPlayerWeaponSlot(client, x);
}


/**
 * 클라이언트로부터 발사물 무기를 명백하게 제거할때 사용합니다.
 *
 * @param client		클라이언트 인덱스
 */
stock Weapon_RemoveClientGrenades(client)
{
	new grenade = GetPlayerWeaponSlot(client, _:Slot_Projectile);
	while (grenade != -1)
	{
		RemovePlayerItem(client, grenade);
		RemoveEdict(grenade);
		
		// 다음 폭탄 찾기.
		grenade = GetPlayerWeaponSlot(client, _:Slot_Projectile);
	}
}


/**
 * 무기를 가져갔다가 되돌려주기
 * (Refresh a weapon by taking it and giving it back)
 *
 * @param client		클라이언트 인덱스
 * @param slot			새로고칠 무기 슬롯 (enum WeaponsSlot 참조)
 */
stock Weapon_RefreshClientWeapon(client, WeaponsSlot:slot)
{
	new weaponindex = GetPlayerWeaponSlot(client, _:slot);
	
	// 무기가 무효하면 정지.
	if (weaponindex == -1)
		return;
	
	// 다시 줄 무기의 클래스네임을 획득
	decl String:entityname[WEAPON_MAX_LENGTH];
	GetEdictClassname(weaponindex, entityname, sizeof(entityname));
	
	// 무기 새로고침
	RemovePlayerItem(client, weaponindex);
	RemoveEdict(weaponindex);
	GivePlayerItem(client, entityname);
}


/**
 * 옵션과 함께 클라이언트한테 칼까지 포함한 모든 무기 제거
 *
 * @param client		클라이언트 인덱스
 */
stock Weapon_RemoveAllClientWeapons(client)
{
	// 모든 클라이언트의 무기 인덱스 목록을 획득
	new weapons[WeaponsSlot];
	Weapon_GetClientWeapons(client, weapons);
	
	// x = 무기 슬롯
	for (new x = 0; x < WEAPON_SLOTS_MAX; x++)
	{
		// 무기가 무효하면 정지
		if (weapons[x] == -1)
			continue;
		
		// 이게 칼 슬롯이라면 빼앗고 정지한다
		if (WeaponsSlot:x == Slot_Melee)
		{
			// 칼 빼앗기
			RemovePlayerItem(client, weapons[x]);
			RemoveEdict(weapons[x]);
			continue;
		}
		
		// 무기 빼앗기
		RemovePlayerItem(client, weapons[x]);
		RemoveEdict(weapons[x]);
	}
	
	// 나머지 발사물 제거.
	Weapon_RemoveClientGrenades(client);
	
	// 새 칼을 지급한다. (If you leave the old one there will be glitches with the knife positioning)
	GivePlayerItem(client, "weapon_knife");
}


/**
 * 모든 클라이언트들의 무기를 가져가고 다시 지급해주어 새로 고칩니다.
 * (Refresh a weapon by taking it and giving it back.)
 *
 * @param client		클라이언트 인덱스
 */
stock Weapon_RefreshAllClientWeapons(client)
{
	// 모든 클라이언트의 무기 인덱스 목록을 획득.
	new weapons[WeaponsSlot];
	Weapon_GetClientWeapons(client, weapons);
	
	// x = 무기 슬롯
	for (new x = 0; x < WEAPON_SLOTS_MAX; x++)
	{
		// 무기가 무효하면 정지.
		if (weapons[x] == -1)
			continue;
		
		// 무기 새로고침
		Weapon_RefreshClientWeapon(client, WeaponSlot:x);
	}
}
