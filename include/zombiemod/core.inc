/*==================================================================

	======================
	 [ZombieMod] Core API
	======================
	
	파일: core.inc
	언어: SourcePawn
	방식: 절차형
	종류: 라이브러리
	설명: 플레이어 구분 확인과 플레이어 구분 설정, 감염,
		  감염 시 호출할 포워드들이 있습니다.

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

/**
 * 클라이언트가 숙주 좀비인가요?
 *
 * @param client		클라이언트 인덱스
 */
native bool IsClientMotherZombie(int client);

/**
 * 클라이언트가 좀비인가요?
 * 
 * @param client		클라이언트 인덱스
 */
native bool IsClientZombie(int client);

/**
 * 클라이언트가 인간인가요?
 * 
 * @param client		클라이언트 인덱스
 */
native bool IsClientHuman(int client);

/**
 * 클라이언트의 속성을 설정(인간, 좀비, 숙주좀비 등)
 *
 * @param client		클라이언트 인덱스
 * @param type			변경할 속성 (zombiemod.inc 참조)
 */
native SetClientType(int client, int type);

/**
 * 클라이언트를 감염시킵니다
 * 
 * @param client		클라이언트 인덱스
 * @param attacker		공격자 인덱스
 * @param mother		설정 시 숙주로 감염
 */
native InfectClient(int client, int attacker = -1, bool mother = false);

/**
 * 클라이언트를 인간으로 되돌려놓습니다
 * 
 * @param client		클라이언트 인덱스
 */
native CureClient(int client);

/**
 * 클라이언트를 감염시키기 전(BEFORE)에 호출
 * 
 * @param client		클라이언트 인덱스
 * @param attacker		공격자 인덱스
 * @param mother		숙주로 감염하려면 설정
 */
forward Action OnInfectClientPre(int &client, int &attacker, bool &mother);

/**
 * 클라이언트를 감염시킨 후(AFTER)에 호출
 * 
 * @param client		클라이언트 인덱스
 * @param attacker		공격자 인덱스
 * @param mother		숙주로 감염하려면 설정
 */
forward OnInfectClientPost(int client, int attacker, bool mother);

/**
 * 클라이언트를 인간으로 만들기 전(BEFORE)에 호출
 * 
 * @param client		클라이언트 인덱스
 */
forward Action OnCureClientPre(int &client);

/**
 * 클라이언트를 인간으로 만든 후(AFTER)에 호출
 * 
 * @param client		클라이언트 인덱스
 */
forward OnCureClientPost(int client);
