/*==================================================================

	=========================
	 [ZombieMod] Base Header
	=========================
	
	파일: common_const.inc
	언어: SourcePawn
	방식: 절차형
	종류: 함수 모음
	설명: 대부분은 Zombie Reloaded에서 가져와 변형함.

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

#include <sdktools>
#include <cstrike>


/**
 * 클라이언트가 유효한 클라이언트인지 확인합니다.
 * 
 * @param client		클라이언트 인덱스
 * @param console		뭔지 모른다.
 */
stock bool IsClientValid(int client, bool console = false)
{
	// 클라이언트 인덱스가 '클라이언트 최대수(MaxClients)보다 높으면 return false.
	if (client < 1 || client > MaxClients)
		return false;
	
	// console이 true인 경우, '클라이언트는 0보다 크거나 같다는 것을 return', 그게 아니면 '클라이언트는 0 보다 크다를 return'
	return console ? (client >= 0) : (client > 0);
}


/**
 * 클라이언트가 아무 팀 안에 들어가있는지를 확인합니다.
 *
 * @param client		클라이언트 인덱스
 * @param team			팀 인덱스
 */
stock bool IsClientOnTeam(int client, int team = -1)
{
	// 클라이언트 인덱스가 무효하면 정지.
	if (!IsClientValid(client))
		return false;
	
	// 클라이언트 팀 획득.
	int clientteam = GetClientTeam(client);
	
	if (team == -1)
		return (clientteam == CS_TEAM_T || clientteam == CS_TEAM_CT);
	
	return (clientteam == team);
}


/**
 * 각 팀에 클라이언트가 들어가있는지를 확인합니다.
 *
 * @param team		팀 인덱스
 */
stock bool TeamHasClients(int team = -1)
{
	// 양 팀이 적어도 1명의 클라이언트를 포함하고있다면 return true.
	if (team == -1)
		return (GetTeamClientCount(CS_TEAM_T) && GetTeamClientCount(CS_TEAM_CT));

	// 주어진 팀에 적어도 1명의 클라이언트가 포함되어있다면 return true.
	return bool GetTeamClientCount(team);
}


/**
 * 유효한 클라이언트의 수를 계산합니다.
 *
 * @param zombiecount			좀비 클라이언트의 수를 저장할 변수
 * @param humancount			인간 클라이언트의 수를 저장할 변수
 * @param alive					오직 살아있는 클라이언트의 수만 계산합니다.
 * @param ignorezombiespawned	좀비가 스폰된 것을 무시하고 클라이언트 수를 셉니다.
 */
stock bool CountValidClients(&zombiecount = 0, &humancount = 0, bool alive = true, bool ignorezombiespawned = false)
{
	/*
		좀비가 스폰되지 않았고 인간만을 세지 않았다면 정지.
		If zombie hasn't spawned and were not only counting humans, then stop
	*/
	if (!IsZombieSpawned() && !ignorezombiespawned)
		return false;
	
	// x = 클라이언트 인덱스
	for (int x = 1; x <= MaxClients; x++)
	{
		// 클라이언트가 in-game 상태가 아님
		if (!IsClientInGame(x))
			continue;
		
		// 팀에 없는 사람 패스
		if (!IsClientOnTeam(x))
			continue;
		
		// 살아있는 사람만
		if (alive && !IsPlayerAlive(x))
			continue;
		
		// 클라이언트가 좀비나 숙주좀비라면 좀비 증가
		if (IsClientZombie(x) || IsClientMotherZombie(x))
			zombiecount++;
		
		// 클라이언트가 인간이면, 인간 중가
		if (IsClientHuman(x))
			humancount++;
	}
	
	return true;
}

/**
 * 배열을 생성하여 좀비가 되기에 적합한 클라이언트를 채워넣습니다.
 *
 * @param arrayEligibleClients		배열의 핸들, 이것이 완료되었을 때 CloseHandle 함수를 호출하는 것을 잊지 마세요.
 * @param team						팀 안에 들어가 있어야만 적합
 * @param alive						살아있어야만 적합
 * @param human						인간이어야만 적합
 */
stock CreateEligibleClientList(&Handle:arrayEligibleClients, bool team = false, bool alive = false, bool human = false)
{
	// 배열 생성.
	arrayEligibleClients = CreateArray();
	
	// 적합한 클라이언트들을 리스트에 채워넣는다.
	// x = 클라이언트 인덱스
	for (int x = 1; x <= MaxClients; x++)
	{
		// 클라이언트가 in-game 상태가 아니면 정지.
		if (!IsClientInGame(x))
			continue;
		
		// 클라이언트가 팀에 들어가있지 않으면 정지.
		if (team && !IsClientOnTeam(x))
			continue;
		
		// 클라이언트가 죽어있다면 정지.
		if (alive && !IsPlayerAlive(x))
			continue;
		
		// 클라이언트가 좀비라면(인간이 아니라면) 정지.
		if (human && !IsClientHuman(x))
			continue;
		
		// 적합한 클라이언트를 집어넣는다.
		PushArrayCell(arrayEligibleClients, x);
	}
	
	return GetArraySize(arrayEligibleClients);
}


/**
 * 인간이 마지막 생존자인지 확인하기 위한 함수
 */
stock void CheckPlayers()
{
	// 인간 수를 셉니다.
	int humancount;
	CountValidClients(_, humancount, true, true);
	
	// 인간이 1명이면 감염 방지
	if (humancount == 1)
		SetInfectStop(true);
}

/**
 * HUD 메시지 출력 (하프라이프 또는 1.6에서 볼 수 있는 스타일의 색깔있는 HUD메시지)
 *
 * @param client 		클라이언트 인덱스
 * @param channel 		표시할 채널 (개발자가 확실하게 정해두고 사용하기.)
 * @param position 		표시할 위치
 * @param firstcolors 	첫번째 색
 * @param secondcolors  두번째 색
 * @param effect 		효과
 						1:
 						2:
 						3:  
 * @param time_fadein	페이드-인 효과 시간
 * @param time_fadeout 	페이드-아웃 효과 시간
 * @param holdtime 		메시지가 지속될 시간
 * @param effecttime 	효과가 지속될 시간
 * @param text 			표시할 문장
 */
stock void PrintColorHUDMessage(int client, int channel, 
							const float position[2], 
							const int firstcolors[4], const int secondcolors[4],
							int effect, float time_fadein, float time_fadeout,
							float holdtime, float effecttime, const char[] text, any:...)
{
	UserMsg iHudMsg = GetUserMessageId("HudMsg");

	int clients[1];
	clients[0] = client;
	Handle hMsg = StartMessageEx(iHudMsg, clients, 1);

	char buffer[256];
	VFormat(buffer, sizeof buffer, text, 12);
	
	// CS:GO 지원.
	if (GetUserMessageType() == UM_Protobuf)
	{
		PbSetInt(hMsg, "channel", channel);
		PbSetVector2D(hMsg, "pos", position);
		PbSetColor(hMsg, "clr1", firstcolors);
		PbSetColor(hMsg, "clr2", secondcolors);
		PbSetInt(hMsg, "effect", effect);
		PbSetFloat(hMsg, "fade_in_time", time_fadein);
		PbSetFloat(hMsg, "fade_out_time", time_fadeout);
		PbSetFloat(hMsg, "hold_time", holdtime);
		PbSetFloat(hMsg, "fx_time", effecttime);
		PbSetString(hMsg, "text", buffer);

		EndMessage();
	}

	// CS:S는 나중에.
}


/**
 * 키힌트 HUD를 이용한 메시지를 출력합니다.
 *
 * @param client	클라이언트 인덱스 (0을 입력할 경우 전체에게 표시합니다.)
 * @param msg		표시할 내용을 입력하세요.
 */
stock void PrintKeyHintText(int client, char[] msg, any:...)
{
	Handle hudhandle = null;
	
	if (client == 0) hudhandle = StartMessageAll("KeyHintText");	
	else hudhandle = StartMessageOne("KeyHintText", client);

	if (hudhandle == null)
		return;
	
	char txt[255];
	VFormat(txt, sizeof(txt), msg, 3);	
	
	if (GetUserMessageType() == UM_Protobuf)
	{
		PbAddString(hudhandle, "hints", txt);
		EndMessage();
		return;
	}

	BfWriteByte(hudhandle, 1);
	BfWriteString(hudhandle, txt);
	EndMessage();
}



/**
 * @section CS1.6 스타일 HUD의 표시 채널 정의
 */
#define FFADE_IN			0x0001		// Just here so we don't pass 0 into the function
#define FFADE_OUT			0x0002		// Fade out (not in)
#define FFADE_MODULATE		0x0004		// Modulate (don't blend)
#define FFADE_STAYOUT		0x0008		// ignores the duration, stays faded out until new ScreenFade message received
#define FFADE_PURGE			0x0010		// Purges all other fades, replacing them with this one
/**
 * @endsection
 */

/**
 * 시각; 클라이언트의 화면 흔들기
 *
 * @param client		클라이언트 인덱스
 * @param amplitude		강도
 * @param frequency		범위
 * @param duration		시간
 */
stock void ShakeClientScreen(int client, float amplitude, float frequency, float duration)
{
	// 화면 흔들기 유저메시지가 유효하지 않다면 정지.
	Handle hShake = StartMessageOne("Shake", client);
	if (hShake == INVALID_HANDLE)
		return;

	// CS:GO 지원
	if (GetUserMessageType() == UM_Protobuf)
	{
		PbSetInt(hShake, "command", 0);
		PbSetFloat(hShake, "local_amplitude", amplitude);
		PbSetFloat(hShake, "frequency", frequency);
		PbSetFloat(hShake, "duration", duration);

		// 유저메시지를 끝내고 클라이언트에게 전송한다.
		EndMessage();
		return;
	}

	// 화면 흔들기 정보를 유저메시지 핸들에 작성한다.
	BfWriteByte(hShake, 0);
	BfWriteFloat(hShake, amplitude);
	BfWriteFloat(hShake, frequency);
	BfWriteFloat(hShake, duration);
	
	// 유저메시지를 끝내고 클라이언트에게 전송한다.
	EndMessage();
}


/**
 * 시각; 페이드 메시지를 실행합니다.
 *
 * @param client	클라이언트 인덱스.
 * @param duration	시간
 * @param holdtime	고정 시간
 * @param fadeflag	페이드 플래그 값
 * @param red		빨강색
 * @param green		초록색
 * @param blue		파랑색
 * @param alpha		투명도
 */
stock void SendFadeMessage(int client, int duration, int holdtime, int fadeflag, int red, int green, int blue, int alpha)
{
	Handle hFade;
	
	if (client == 0)
		hFade = StartMessageAll("Fade");
	
	else hFade = StartMessageOne("Fade", client);
	
	// CS:GO 지원
	if (GetUserMessageType() == UM_Protobuf)
	{
		PbSetInt(hFade, "duration", duration);
		PbSetInt(hFade, "hold_time", holdtime);
		PbSetInt(hFade, "flags", fadeflag);
		PbSetColor(hFade, "clr", {red, green, blue, alpha});
		EndMessage();
		return;
	}

	BfWriteShort(hFade, duration);
	BfWriteShort(hFade, holdtime);
	BfWriteShort(hFade, fadeflag);
	BfWriteByte(hFade, red);
	BfWriteByte(hFade, green);
	BfWriteByte(hFade, blue);
	BfWriteByte(hFade, alpha);
	
	EndMessage();
}

/**
 * @section Explosion flags.
 */
#define EXP_NODAMAGE               1
#define EXP_REPEATABLE             2
#define EXP_NOFIREBALL             4
#define EXP_NOSMOKE                8
#define EXP_NODECAL               16
#define EXP_NOSPARKS              32
#define EXP_NOSOUND               64
#define EXP_RANDOMORIENTATION    128
#define EXP_NOFIREBALLSMOKE      256
#define EXP_NOPARTICLES          512
#define EXP_NODLIGHTS           1024
#define EXP_NOCLAMPMIN          2048
#define EXP_NOCLAMPMAX          4096
/**
 * @endsection
 */

/**
 * Create an explosion effect with strict flags.
 * 
 * @param origin    The (x, y, z) coordinate of the explosion.
 * @param flags     The flags to set on the explosion.
 */
stock void SetupExplosion(const float origin[3], flags)
{
	// Create an explosion entity.
	new explosion = CreateEntityByName("env_explosion");

	// If explosion entity isn't valid, then stop.
	if (explosion == -1)
		return;

	// Get and modify flags on explosion.
	new spawnflags = GetEntProp(explosion, Prop_Data, "m_spawnflags");
	spawnflags = spawnflags | EXP_NODAMAGE | EXP_NODECAL | flags;

	// Set modified flags on entity.
	SetEntProp(explosion, Prop_Data, "m_spawnflags", spawnflags);

	// Spawn the entity into the world.
	DispatchSpawn(explosion);

	// Set the origin of the explosion.
	DispatchKeyValueVector(explosion, "origin", origin);

	// Set fireball material.
	PrecacheModel("materials/sprites/xfireball3.vmt");
	DispatchKeyValue(explosion, "fireballsprite", "materials/sprites/xfireball3.vmt");

	// Tell the entity to explode.
	AcceptEntityInput(explosion, "Explode");

	// Remove entity from world.
	RemoveEdict(explosion);
}
