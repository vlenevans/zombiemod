/*==================================================================

	==========================================
	 [ZombieMod] CS:GO Flashlight Alternative
	==========================================
	
	파일: zm_csgo_flashlight.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: Flashlight가 되지 않는 CS:GO 에서
		  되게끔 해줍니다.

	제작: Vlen
	웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <zombiemod>


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] CS:GO Flashlight",
	author 		= ZM_PRODUCER,
	description = "Flashlight for CS:GO",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}
	

//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작시
 */
public void OnPluginStart()
{
	// 클라이언트 명령어 : F키
	AddCommandListener(Command_LookAtWeapon, "+lookatweapon");
	
	// 서버에 알림
	PrintToServer("%s Enable :: Manager :: CS:GO Flashlight", ZM_LANG_PREFIX);
}


//============================================================
// 콜백
//============================================================
/**
 * 게임이벤트; 라운드 시작
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public Action Command_LookAtWeapon(int client, char[] command, int argc)
{
	flash(client);

	return Plugin_Handled;
}


//============================================================
// 일반
//============================================================
/**
 * 플래시 : ON/OFF
 * 
 * @param client 	클라이언트 인덱스
 */
void flash(int client)
{
	// 플래시라이트 켜기/끄기
	any effect = GetEntProp(client, Prop_Send, "m_fEffects");
	SetEntProp(client, Prop_Send, "m_fEffects", effect ^ 4);

	// 소리 재생.
	PrecacheSound("items/flashlight1.wav");
	EmitSoundToClient(client, "items/flashlight1.wav");
}
