/*==================================================================

	================================================
	 [ZombieMod] Server Protection for CS:GO Server
	================================================
	
	파일: zm_csgo_server_protect.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: 플레이어가 관전자 팀 외에 다른 팀으로 들어가고 변경됨을
		  알리는 메시지 출력을 막습니다.
		  그리고 라운드 종료 시 팀 인원 균형을 맞춥니다.

	개정: CS:GO를 위하여 팀 이름 조정기능을 추가했음. (2018/June/7)

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <cstrike>
#include <zombiemod>


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Server Protection for CS:GO",
	author 		= ZM_PRODUCER,
	description = "Prevent a suicide-spammer crashes server for CS:GO server",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 변수
//============================================================
/*
# Prevent a kick-spammer explodes the server.
mp_disable_autokick 1
mp_autokick 0
*/
ConVar cvDisableAutoKick = null;
ConVar cvAutoKick = null;


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작 시
 */
public void OnPluginStart()
{
	// 서버 명령어 찾기.
	cvDisableAutoKick = FindConVar("mp_disable_autokick");
	cvAutoKick = FindConVar("mp_autokick");
	
	// 서버에 알림
	PrintToServer("%s Enable :: Manager :: CS:GO Server Protection", ZM_LANG_PREFIX);
}

/**
 * 맵 시작 시
 */
public void OnMapStart()
{
	// 서버 명령어 조정.
	cvDisableAutoKick.SetInt(1);
	cvAutoKick.SetInt(0);
}
