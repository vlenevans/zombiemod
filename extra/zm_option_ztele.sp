/*==================================================================

	==============================
	 [ZombieMod] ZTELE (Teleport)
	==============================
	
	파일: zm_ztele.sp
	언어: SourcePawn
    방식: 절차형
    종류: 모듈
	설명: 끼었거나 행동에 지장이 있는 경우를 위한 텔레포트 기능.
		  인간과 좀비 모두가 사용 가능하나, 사용 횟수의 제한있음.

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <zombiemod>


//============================================================
// 변수
//============================================================
// 서버 명령어
enum CVAR
{
	Handle:ZTELEHUMANCOUNT,
	Handle:ZTELEZOMBIECOUNT
}
new cv[CVAR];

new Float:g_vecZTeleSpawn[MAXPLAYERS + 1][3];	// 클라이언트 포지션 저장
new Float:g_vecZTeleOrigin[MAXPLAYERS + 1][3];	// 클라이언트 원 위치 저장

new g_iZTeleHumanCount[MAXPLAYERS + 1];			// 인간이 텔레포트를 한 횟수 저장.
new g_iZTeleZombieCount[MAXPLAYERS + 1];		// 좀비가 텔레포트를 한 횟수 저장.


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] ZTele",
	author 		= ZM_PRODUCER,
	description = "ZTele.",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작 시
 */
public void OnPluginStart()
{
	// 서버 명령어
	cv[ZTELEHUMANCOUNT] = CreateConVar("zm_ztele_human_limit_count", "1", "인간의 ztele 명령어 사용 가능 횟수.");
	cv[ZTELEZOMBIECOUNT] = CreateConVar("zm_ztele_zombie_limit_count", "3", "좀비의 ztele 명령어 사용 가능 횟수.");
	
	// 콘솔 명령어
	RegConsoleCmd("say", Command_Say);
	RegConsoleCmd("team_say", Command_Say);
	RegConsoleCmd("ztele", Command_Teleport);
	RegConsoleCmd("teleport", Command_Teleport);
	
	// 게임 이벤트
	HookEvent("player_spawn", Event_PlayerSpawn);
	
	// 서버에 알림
	PrintToServer("%s Enable :: Module :: ZTele", ZM_LANG_PREFIX);
}


//============================================================
// 콜백
//============================================================
/**
 * 서버 명령어: 클라이언트가 텔레포트 사용시
 * 
 * @param client		클라이언트 인덱스
 * @param args 			인자
 */
public Action Command_Say(int client, int args)
{
	// 이 작업은 왜하는건지 -_-;
	decl String:says[192];
	GetCmdArgString(says, sizeof(says));
	ReplaceString(says, sizeof(says), "\"", "");
	
	// 채팅에 아래와 같은 명령어 하나만이라도 사용할 경우
	if (StrEqual(says, "!텔레포트") || StrEqual(says, "!텔"))
	{
		// 동작
		ZTele_DoTeleport(client);
	}
}

/**
 * 서버 명령어: 클라이언트가 텔레포트 사용시 2
 * 
 * @param client		클라이언트 인덱스
 * @param args 			인자
 */
public Action Command_Teleport(int client, int args)
{
	// 동작
	ZTele_DoTeleport(client);
}

/**
 * 게임 이벤트; 플레이어 스폰 시
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public Action Event_PlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	ZTele_OnClientSpawn(client);
}


//============================================================
// 일반
//============================================================
/**
 * 텔레포트 모듈; 클라이언트가 스폰했을 때
 * 
 * @param client		클라이언트 인덱스
 */
void ZTele_OnClientSpawn(int client)
{
	// 텔레포트한 횟수 초기화.
	g_iZTeleHumanCount[client] = GetConVarInt(cv[ZTELEHUMANCOUNT]);
	g_iZTeleZombieCount[client] = GetConVarInt(cv[ZTELEZOMBIECOUNT]);
	
	// 클라이언트의 위치 획득.
	GetClientAbsOrigin(client, g_vecZTeleSpawn[client]);
}

/**
 * 텔레포트 모듈; 클라이언트에게 텔레포트모듈을 가동합니다.
 * 
 * @param client		클라이언트 인덱스
 */
void ZTele_DoTeleport(int client)
{
	// 현재 위치 획득.
	GetClientAbsOrigin(client, g_vecZTeleOrigin[client]);
	
	// 살아있지 않으면 정지.
	if (!IsPlayerAlive(client))
	{
		PrintToChat(client, "\x05[CZ] \x03살아있지 않습니다.");
		return;
	}
	
	// 클라이언트가 좀비이면
	if (IsClientZombie(client) || IsClientMotherZombie(client))
	{
		// 텔레포트 카운트가 0보다 작으면 정지.
		if (g_iZTeleZombieCount[client] < 0)
		{
			PrintToChat(client, "\x05[CZ] \x03당신은 모든 텔레포트를 썼습니다.");
			return;
		}
		
		// 그게 아니면 텔레포트하고 텔레포트 가능 횟수 감소.
		else 
		{
			// 텔레포트.
			ZTele_TeleportClient(client);
			
			// 사용 가능 횟수 감소.
			g_iZTeleZombieCount[client]--;
			
			// 해당 클라이언트에게 알림.
			PrintToChat(client, "\x05[CZ] \x03%d번의 텔레포트 권한이 남았습니다.", g_iZTeleZombieCount[client]);
		}
	}
	
	// 클라이언트가 인간이면
	else if (IsClientHuman(client))
	{
		// 텔레포트 카운트가 0보다 작으면 정지.
		if (g_iZTeleHumanCount[client] < 0)
		{
			PrintToChat(client, "\x05[CZ] \x03당신은 모든 텔레포트를 썼습니다.");
			return;
		}
		
		// 그게 아니면 텔레포트하고 텔레포트 가능 횟수 감소.
		else 
		{
			// 텔레포트.
			ZTele_TeleportClient(client);
			
			// 사용 가능 횟수 감소.
			g_iZTeleHumanCount[client]--;
			
			// 해당 클라이언트에게 알림.
			PrintToChat(client, "\x05[CZ] \x03%d번의 텔레포트 권한이 남았습니다.", g_iZTeleZombieCount[client]);
		}
	}
}

/**
 * 텔레포트 모듈; 클라이언트를 텔레포트합니다.
 *
 * @param client		클라이언트 인덱스
 */
void ZTele_TeleportClient(int client)
{
	// 텔레포트!
	TeleportEntity(client, g_vecZTeleSpawn[client], NULL_VECTOR, NULL_VECTOR);
}
