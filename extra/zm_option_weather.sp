/*==================================================================

	===============================
	 [ZombieMod] Option :: Weather
	===============================
	
	파일: zm_option_weather.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: 맵 내의 날씨를 관리합니다. 햇빛차단은 동작하지 않습니다.
	
	제작: Vingbbit
	웹: http://github.com/vlennus

==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <zombiemod>


//============================================================
// 변수
//============================================================
// 서버 명령어
enum CV
{
	Handle:SKYSET,
	Handle:SKYPATH,
	Handle:LIGHTSET,
	Handle:LIGHTVALUE,
	Handle:DISABLESUN
};

new cv[CV];

// 기본 하늘
new String:g_vAmbienceDefaultSky[PLATFORM_MAX_PATH];


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Weather Manager",
	author 		= ZM_PRODUCER,
	description = "Manages the map environment(weather)",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작시
 */
public void OnPluginStart()
{
	cv[SKYSET] = CreateConVar("zm_sky", "1", "Enable/Disable replaces the skybox.\n스카이박스 교체를 활성화/비활성화 합니다.");
	
	if (GetEngineVersion() != Engine_CSGO)
		cv[SKYPATH] = CreateConVar("zm_skybox", "sky_borealis01", "skybox name to replace (Must be zp_sky is 1.)\n교체될 스카이박스의 이름입니다. (zm_sky가 1이어야 합니다.)");
	else if (GetEngineVersion() == Engine_CSGO)
		cv[SKYPATH] = CreateConVar("zm_skybox", "sky_csgo_night02b", "skybox name to replace (Must be zp_sky is 1.)\n교체될 스카이박스의 이름입니다. (zm_sky가 1이어야 합니다.)");
	
	cv[LIGHTSET] = CreateConVar("zm_light", "1", "Changes the map light style\n맵 전체의 밝기를 변경합니다.");
	cv[LIGHTVALUE] = CreateConVar("zm_lightvalue", "g", "The light style value (Must be zp_light is 1)\n밝기 값 (zm_light가 1이어야 합니다.)");
	cv[DISABLESUN] = CreateConVar("zm_disable_sun", "1", "Remove entity the sun (doesn't work.)\n태양을 제거합니다 (먹히지 않습니다.)");
	
	EnvCvarHook();
	
	// 서버에 알림
	PrintToServer("%s Enable :: Option :: Weather Manager", ZM_LANG_PREFIX);
}

/**
 * 맵 시작시
 */
public void OnMapStart()
{
	// 날씨, 환경 모두 적용
	ApplyAll();
	
	// 커스텀 설정이 꺼저있으면 정지
	bool sky = GetConVarBool(cv[SKYSET]);
	if (!sky)
		return;
	
	char downloadpath[PLATFORM_MAX_PATH];
	char path[PLATFORM_MAX_PATH];
	
	// 스카이박스 경로 획득.
	GetConVarString(cv[SKYPATH], path, sizeof(path));
	
	// 다운로드 경로 포맷.
	Format(downloadpath, sizeof(downloadpath), "materials/skybox/%s", path);
	
	// 다운로드 테이블에 추가.
	AddFileToDownloadsTable(downloadpath);
	
	// 재료 불러오기.
	//StuffsLoad();
}

/**
 * 맵 종료시
 */
public void OnMapEnd()
{
	g_vAmbienceDefaultSky[0] = 0;
}


//============================================================
// 호출
//============================================================
public void EnvCvarHookLightStyle(Handle cvar, const char[] oldvalue, const char[] newvalue)
{
	// 조명설정 콘바 값이 비활성화 됐다면 비활성화.
	bool lightset = GetConVarBool(cv[LIGHTSET]);
	
	// 조명 설정 적용.
	ApplyLightStyle(!lightset);
}

public EnvCvarHookSkybox(Handle cvar, const char[] oldvalue, const char[] newvalue)
{
	// 스카이박스 설정 콘바 값이 비활성화 됐다면, 비활성화.
	bool sky = GetConVarBool(cv[SKYSET]);
	
	// 스카이박스 적용.
	ApplySky(!sky);
}

public EnvCvarHookDisableSun(Handle cvar, const char[] oldvalue, const char[] newvalue)
{
	// 태양 설정 콘바 값이 비활성화 됐다면, 비활성화.
	bool sun = GetConVarBool(cv[DISABLESUN]);
	
	// 태양 적용.
	DisableSun(!sun);
}


//============================================================
// 일반
//============================================================
void EnvCvarHook(bool unhook = false)
{
	// unhook이 true면 계속한다.
	if (unhook)
	{
		// 조명 설정 콘바들을 unhook
		UnhookConVarChange(cv[LIGHTSET], EnvCvarHookLightStyle);
		UnhookConVarChange(cv[LIGHTVALUE], EnvCvarHookLightStyle);
		
		// 스카이박스 설정 콘바들을 unhook.
		UnhookConVarChange(cv[SKYSET], EnvCvarHookSkybox);
		UnhookConVarChange(cv[SKYPATH], EnvCvarHookSkybox);
		
		// 태양설정 콘바를 unhook.
		UnhookConVarChange(cv[DISABLESUN], EnvCvarHookDisableSun);
		
		// unhook 후 정지.
		return;
	}
	
	// 조명 설정 콘바들을 hook
	HookConVarChange(cv[LIGHTSET], EnvCvarHookLightStyle);
	HookConVarChange(cv[LIGHTVALUE], EnvCvarHookLightStyle);
	
	// 스카이박스 설정 콘바들을 hook.
	HookConVarChange(cv[SKYSET], EnvCvarHookSkybox);
	HookConVarChange(cv[SKYPATH], EnvCvarHookSkybox);
	
	// 태양설정 콘바를 hook
	HookConVarChange(cv[DISABLESUN], EnvCvarHookDisableSun);
}

/**
 * 환경; 모든 것을 적용합니다.
 */
void ApplyAll()
{
	// 조명 설정 적용.
	bool lightstyle = GetConVarBool(cv[LIGHTSET]);
	ApplyLightStyle(!lightstyle);
	
	// 스카이박스 설정 적용.
	bool sky = GetConVarBool(cv[SKYSET]);
	ApplySky(!sky);
}

/**
 * 환경; 조명스타일을 적용합니다.
 * 
 * @param disable		이를 true로 할 경우 조명스타일을 적용하지 않습니다.
 */
void ApplyLightStyle(bool disable = false)
{
	// 비활성화되어있으면 기본 조명으로 설정 후 정지.
	if (disable)
	{
		SetLightStyle(0, "n");
		return;
	}
	
	// 조명 값 얻기.
	decl String:lightvalue[4];
	GetConVarString(cv[LIGHTVALUE], lightvalue, sizeof(lightvalue));
	
	// 그 값으로 조명 설정.
	SetLightStyle(0, lightvalue);
}

/**
 * 환경; 스카이박스를 적용합니다.
 *
 * @param disable		이를 true로 할 경우 스카이박스를 적용하지 않습니다.
 */
void ApplySky(bool disable = false)
{
	// 스카이박스 이름을 찾을 수 없으면 정지.
	ConVar hSkyname = FindConVar("sv_skyname");
	if (hSkyname == null)
		return;
	
	// 맵의 기본 스카이박스를 새로운 스카이박스가 적용되기 전에 저장합니다.
	if (!g_vAmbienceDefaultSky[0])
		GetConVarString(hSkyname, g_vAmbienceDefaultSky, sizeof(g_vAmbienceDefaultSky));
	
	// 비활성화되어있다면 해당 맵의 기본 스카이박으로 설정 후 정지.
	if (disable)
	{
		if (g_vAmbienceDefaultSky[0])
		{
			// 기본 스카이박스를 모든 클라이언트들에게 설정한다.
			SetConVarString(hSkyname, g_vAmbienceDefaultSky, true);
		}
		
		return;
	}
	
	// 스카이박스 경로 획득.
	char skypath[PLATFORM_MAX_PATH];
	GetConVarString(cv[SKYPATH], skypath, sizeof(skypath));
	
	// 새 스카이박스를 모든 클라이언트들에게 적용한다.
	SetConVarString(hSkyname, skypath, true);
}

/**
 * 태양 제거
 *
 * @param disable	이를 활성화할 경우 태양을 제거하지 않습니다.
 */
void DisableSun(bool disable = false)
{
	// 태양 찾기.
	int sun = FindEntityByClassname(-1, "env_sun");
	
	// 엔티티 무효
	if (sun == -1)
		return;
	
	// 비활성화되어있으면 태양을 활성화 후 정지.
	if (disable)
	{
		// 태양 렌더링 활성화.
		AcceptEntityInput(sun, "TurnOn");
		return;
	}
	
	// 태양 렌더링 비활성화.
	AcceptEntityInput(sun, "TurnOff");
}
