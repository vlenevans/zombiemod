/*==================================================================
	
	=============================
	 [ZombieMod] File Downloader
	=============================
	
	파일: zm_filedownloader.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: 필요한 파일을 다운로드 시킵니다.
	
	제작: Vingbbit
	웹: http://github.com/vlennus

==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <zombiemod>


//============================================================
// 변수
//============================================================
// 서버 명령어
new Handle:cv_enable;


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] File Downloader",
	author 		= ZM_PRODUCER,
	description = "Downloads the resource file for ZP Only",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작시
 */
public void OnPluginStart()
{
	// 서버 명령어
	cv_enable = CreateConVar("zm_download_file", "1", "Enable/Disable Downloader\n다운로더를 작동시킵니다.", _, true, 0.0, true, 1.0);
	
	// 서버에 알림
	PrintToServer("%s File Downloader is enable.", ZM_LANG_PREFIX);
}

/**
 * 맵 시작시
 */
public void OnMapStart()
{
	// 다운로드 데이터 불러오기.
	// 켜져있나요?
	if (!GetConVarBool(cv_enable))
		return;
	
	// 파일(경로) 생성
	decl String:path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof(path), "configs/zombiemod/zm_downloads.txt");
	
	// 파일을 열기 위한 핸들 생성.
	new Handle:hFileDownload = OpenFile(path, "r");
	
	// 무효하다면 실패로그 생성.
	if (hFileDownload == INVALID_HANDLE)
		SetFailState("%s \"%s\" missing from server", ZM_LANG_PREFIX, path);
	
	// 다운로드 배열 생성.
	new Handle:hArrayDownload = CreateArray(256, 1);
	
	FileLinesToArray(hArrayDownload, hFileDownload);
	
	char file[256];
	
	// 다운로드 사이즈?
	int downloadsize = GetArraySize(hArrayDownload);
	for (new x = 0; x < downloadsize; x++)
	{
		GetArrayString(hArrayDownload, x, file, sizeof(file));
		if (FileExists(file)) AddFileToDownloadsTable(file);
		else LogError("[ZP] File Load failed. (%s)", file);
	}
	
	CloseHandle(hFileDownload);
	CloseHandle(hArrayDownload);
}


//============================================================
// 일반
//============================================================
stock void FileLinesToArray(Handle hArray, const Handle hFile)
{
	// 배열 청소.
	ClearArray(hArray);
	
	decl String:line[128];
	while (!IsEndOfFile(hFile) && ReadFileLine(hFile, line, sizeof(line)))
	{
		if (StrContains(line, ";") == -1)
		{
			if (StrContains(line, "//") > -1)
				SplitString(line, "//", line, sizeof(line));
			
			TrimString(line);
			
			if (!StrEqual(line, "", false))
				PushArrayString(hArray, line);
		}
	}
}
