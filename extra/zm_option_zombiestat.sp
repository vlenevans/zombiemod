/*==================================================================

	==========================
	 [ZombieMod] Zombie Stat
	==========================
	
	파일: zm_option_zombiestat.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: 클래스가 별도로 없는 경우 좀비 능력치를 임의로 지정합니다.
		  클래스 기능을 적용했다면 이 플러그인을 서버에서 제거하세요!

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <zombiemod>


//============================================================
// 상수
//============================================================
#define HUMAN_DEFAULT_HP 	150
#define ZOMBIE_HP_MIN 		500
#define ZOMBIE_HP_MAX 		6500


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Zombie Stat",
	author 		= ZM_PRODUCER,
	description = "Zombie's color, other stat!",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작 시
 */
public void OnPluginStart()
{
	// 서버에 알림
	PrintToServer("%s Enable :: Option :: Zombie Stat", ZM_LANG_PREFIX);
}


//============================================================
// 콜백
//============================================================
/**
 * 플레이어가 감염된 후(Post)
 *
 * @param client 			피해자 인덱스
 * @param attacker			공격자 인덱스
 * @param mother	 		숙주였나요?
 */
public OnInfectClientPost(int client, int attacker, bool mother)
{
	if (!IsClientValid(client))
		return;

	if (!IsClientInGame(client))
		return;

	if (!IsPlayerAlive(client))
		return;

	// 빠 바 빨간맛!
	SetEntityRenderColor(client, 255, 0, 0, 255);

	// 체력 랜덤 설정
	SetEntityHealth(client, GetRandomInt(ZOMBIE_HP_MIN, ZOMBIE_HP_MAX));
}

/**
 * 플레이어가 인간이 된 후(Post)
 *
 * @param client 			피해자 인덱스
 */
public OnCureClientPost(int client)
{
	if (!IsClientValid(client))
		return;
		
	if (!IsClientInGame(client))
		return;

	if (!IsPlayerAlive(client))
		return;
		
	SetEntityRenderColor(client, 255, 255, 255, 255);

	// 인간 기본 체력으로 설정
	SetEntityHealth(client, HUMAN_DEFAULT_HP);
}
