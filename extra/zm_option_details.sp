/*==================================================================

	==========================
	 [ZombieMod] Details
	==========================
	
	파일: zm_option_details.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: 나머지 세부조정을 위한 작업들이 있는 파일.

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <zombiemod>


//============================================================
// 상수
//============================================================
//HUD메시지 출력 채널
#define CHAN_HUD_ATTACKINGZOMBIE 2
#define CHAN_HUD_INFECTINFO 3

// 출력 위치
#define HUD_POS_X 0.7
#define HUD_POS_Y 0.55

// 출력 색깔
#define COLOR1 {200, 0, 0, 200}
#define COLOR2 {170, 0, 0, 200}

#define COLOR_DMG {200, 200, 10, 200}


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Details",
	author 		= ZM_PRODUCER,
	description = "Mod details",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작 시
 */
public void OnPluginStart()
{
	//게임 이벤트
	HookEvent("round_freeze_end", RoundFreezeEnd);

	// 서버에 알림
	PrintToServer("%s Enable :: Option :: Details Displayer", ZM_LANG_PREFIX);
}

/**
 * 클라이언트 서버 접속 시
 *
 * @param client 		클라이언트 인덱스.
 */
public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_OnTakeDamage, SDKH_OnTakeDamage);
}

/**
 * 클라이언트 서버 퇴장 시
 *
 * @param client 		클라이언트 인덱스.
 */
public void OnClientDisconnect(int client)
{
	SDKUnhook(client, SDKHook_OnTakeDamage, SDKH_OnTakeDamage);
}


//============================================================
// 콜백
//============================================================
/**
 * 게임 이벤트: round_freeze_end
 * 
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정 시 알리지 않습니다.
 */
public Action RoundFreezeEnd(Event event, const char[] name, bool dontBroadcast)
{
	for (int x = 1; x <= MaxClients; x++)
	{
		if (!IsClientInGame(x))
			continue;

		float printPosition[2];
		printPosition = view_as<float>({0.3, 0.4});
		PrintColorHUDMessage(x, CHAN_HUD_INFECTINFO, 
						printPosition,
						{0, 0, 255, 200}, {0, 0, 255, 200}, 1,
						0.1, 0.5,
						5.0, 0.2,
						"10초 후 첫번째 좀비가 나타납니다...");
	}
}

/**
 * 플레이어가 감염된 전(Pre)
 *
 * @param client 			피해자 인덱스
 * @param attacker			공격자 인덱스
 * @param mother	 		숙주였나요?
 */
public Action OnInfectClientPre(&client, &attacker, bool &mother)
{
	// 인간팀 숫자 1명이면 감염 정지
	if (GetTeamClientCount(CS_TEAM_CT) < 2)
		return Plugin_Handled;

	return Plugin_Continue;
}

/**
 * 플레이어가 감염된 후(Post)
 *
 * @param client 			피해자 인덱스
 * @param attacker			공격자 인덱스
 * @param mother	 		숙주였나요?
 */
public OnInfectClientPost(int client, int attacker, bool mother)
{
	if (!IsClientValid(attacker))
		return;

	if (!IsClientValid(client))
		return;

	float printPosition[2];
	printPosition = view_as<float>({HUD_POS_X, HUD_POS_Y});

	for (int x = 1; x <= MaxClients; x++)
	{
		if (!IsClientInGame(x))
			continue;
	
		PrintColorHUDMessage(x, CHAN_HUD_INFECTINFO, 
							printPosition,
							COLOR1, COLOR2, 1,
							0.1, 0.5,
							5.0, 0.2,
							"%N님이 %N님을 감염시켰습니다!", attacker, client);
	}

	PrintToChatAll("\x04%N\x05님이 \x04%N\x04님을 \x01감염시켰습니다!", attacker, client);
}

/**
 * SDKHook : OnTakeDamage
 */
public Action SDKH_OnTakeDamage(int client, int &attacker, int &inflictor, float &damage, int &damagetype, int &weapon, 
        float damageForce[3], float damagePosition[3], int damagecustom)
{
	if (!IsClientValid(attacker))
		return Plugin_Continue;

	if (!IsClientValid(client))
		return Plugin_Continue;

	if (GetClientTeam(attacker) == GetClientTeam(client))
		return Plugin_Continue;

	int remainhp = GetClientHealth(client);

	// 공격자에게 출력
	if (damage >= 1) {
		float printPosition[2];
		printPosition = view_as<float>({0.35, 0.70});

		char race[32];
		if (IsClientZombie(client))
			race = "감염자";
		else
			race = "인간";

		PrintColorHUDMessage(attacker, CHAN_HUD_ATTACKINGZOMBIE, 
						printPosition,
						COLOR_DMG, COLOR_DMG, 1,
						0.1, 0.5,
						5.0, 0.2,
						"대상: %N : %s (%.0f / %d)", client, race, damage, remainhp);
	}

	return Plugin_Continue;
}
